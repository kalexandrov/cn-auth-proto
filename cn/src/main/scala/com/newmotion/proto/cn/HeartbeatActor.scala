package com.newmotion.proto.cn

import akka.actor._
import com.spingo.op_rabbit.Directives._
import com.spingo.op_rabbit._

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.duration._

object HeartbeatActor {

  case class Heartbeat(exchange: String)

  case class Timeout(exchange: String)

  case class Subscribe(actor: ActorRef)

  case class KnownExchanges(exchanges: Set[String])

  def apply(rabbitControl: ActorRef)(implicit actorSystem: ActorSystem) = {
    implicit val recoveryStrategy = RecoveryStrategy.none

    val heartbeatActor = actorSystem.actorOf(Props[HeartbeatActor])
    /*val heartbeatSub = */Subscription.run(rabbitControl) {
      channel(qos=3) {
        consume(Binding.fanout(queue("heartbeat"), Exchange.fanout("heartbeat"))) {
          body(as[String]) { exchange =>
            heartbeatActor ! Heartbeat(exchange)
            ack
          }
        }
      }
    }
    heartbeatActor
  }
}

class HeartbeatActor extends Actor with ActorLogging {
  import HeartbeatActor._

  private val heartbeatTimeout = 10.seconds

  def receive = handler(Map.empty, List.empty)

  def handler(knownExchanges: Map[String, Cancellable], actorsToInform: Seq[ActorRef]): Receive = {
    case Heartbeat(ex) =>
      log.info(s"<<< Received heartbeat from $ex")
      knownExchanges.get(ex).foreach(_.cancel())
      val newExchange = ex -> context.system.scheduler.scheduleOnce(heartbeatTimeout, self, Timeout(ex))
      val newExchanges = knownExchanges + newExchange
      context.become(handler(newExchanges, actorsToInform))
      actorsToInform.foreach(_ ! KnownExchanges(newExchanges.keySet))

    case Timeout(ex) =>
      log.info(s"!!! Removing inactive exchange $ex")
      val newExchanges = knownExchanges - ex
      context.become(handler(newExchanges, actorsToInform))
      actorsToInform.foreach(_ ! KnownExchanges(newExchanges.keySet))

    case Subscribe(actor) =>
      context.become(handler(knownExchanges, actorsToInform :+ actor))
  }
}