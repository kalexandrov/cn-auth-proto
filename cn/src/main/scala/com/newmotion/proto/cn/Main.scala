package com.newmotion.proto.cn

import akka.actor._
import akka.pattern.ask
import com.rabbitmq.client.Address
import com.spingo.op_rabbit._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.duration._
import scala.io.StdIn

object Main extends App {
  val log = LoggerFactory.getLogger(getClass)

  log.info("Starting CN")

  implicit val actorSystem = ActorSystem("app")

  val connectionParams = ConnectionParams(
    hosts = new Address("localhost", 5672) :: Nil,
    connectionTimeout = 30000,
    username = "guest",
    password = "guest",
    virtualHost = "cn-auth",
    ssl = false
  )
  val rabbitControl = actorSystem.actorOf(
    Props(new RabbitControl(connectionParams)),
    "rabbit-control"
  )
  val heartbeatActor = HeartbeatActor(rabbitControl)

  val authActor = AuthorizationActor(rabbitControl)

  heartbeatActor ! HeartbeatActor.Subscribe(authActor)

  implicit val askTimeout = akka.util.Timeout(30.seconds)
  Iterator
    .continually(StdIn.readLine("Token (empty string to exit):\n"))
    .takeWhile(_.nonEmpty)
    .foreach(t =>
      (authActor ? AuthorizationActor.AuthorizationRequest(t))
        .onComplete(x => log.info(s"<<< Authorization response for $t is $x"))
    )

  actorSystem.terminate()
}
