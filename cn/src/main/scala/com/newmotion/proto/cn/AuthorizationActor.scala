package com.newmotion.proto.cn

import akka.actor._
import akka.pattern.ask
import com.rabbitmq.client.MessageProperties
import com.spingo.op_rabbit.Directives._
import com.spingo.op_rabbit.Message.ConfirmResponse
import com.spingo.op_rabbit._

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object AuthorizationActor {

  case class AuthorizationRequest(id: String)

  case class Authorized(id: String, exchange: String)

  case class Rejected(id: String)

  case class DeliveryFailed(id: String, reason: String)

  case class AuthorizationTimeout(id: String)

  case class ResponseFromExchange(id: String, exchange: String, authorized: Boolean)

  def apply(rabbitControl: ActorRef)(implicit actorSystem: ActorSystem) = {
    implicit val recoveryStrategy = RecoveryStrategy.none

    val authActor = actorSystem.actorOf(Props(new AuthorizationActor(rabbitControl)))
    /*val authResponseSub = */Subscription.run(rabbitControl) {
      channel(qos=3) {
        consume(Binding.fanout(queue("auth-response"), Exchange.fanout("auth-response"))) {
          body(as[String]) { response =>
            val Array(id, ex, isAuthorized) = response.split(",", -1)
            authActor ! ResponseFromExchange(id, ex, isAuthorized == "yes")
            ack
          }
        }
      }
    }
    authActor
  }
}

class AuthorizationActor(rabbitControl: ActorRef) extends Actor with ActorLogging {
  import AuthorizationActor._

  implicit val askTimeout = akka.util.Timeout(30.seconds)
  private val requestTimeout = 3.seconds

  val publisher = Publisher.exchange(Exchange.direct("auth-request"), "all")

  case class Request(
    sender: ActorRef,
    timeout: Cancellable,
    waitingForExchanges: Set[String]
  )

  def receive = handler(Set.empty, Map.empty)

  def handler(exchanges: Set[String], requests: Map[String, Request]): Receive = {
    case HeartbeatActor.KnownExchanges(ex) =>
      context.become(handler(ex, requests))

    case AuthorizationRequest(id) =>
      log.info(s">>> Received authorization request $id, waiting for $exchanges")
      val newRequest = id -> Request(
        sender,
        context.system.scheduler.scheduleOnce(requestTimeout, self, AuthorizationTimeout(id)),
        exchanges
      )
      context.become(handler(exchanges, requests + newRequest))
      (rabbitControl ? Message(id, publisher)).mapTo[ConfirmResponse]
        .onComplete {
          case Success(Message.Ack(_)) =>
          case Success(Message.Nack(_)) =>
            self ! DeliveryFailed(id, "Unable to send authorization request")
          case Success(Message.Fail(_, ex)) =>
            self ! DeliveryFailed(id, ex.getMessage)
          case Failure(ex) =>
            self ! DeliveryFailed(id, ex.getMessage)
        }

    case AuthorizationTimeout(id) =>
      log.info(s"!!! Authorization request $id timed out")
      requests.get(id).foreach { r =>
        r.sender ! Rejected(id)
      }
      context.become(handler(exchanges, requests - id))

    case DeliveryFailed(id, reason) =>
      log.info(s"!!! Authorization request delivery failed for $id: $reason")
      requests.get(id).foreach { r =>
        r.timeout.cancel()
        r.sender ! Rejected(id)
      }
      context.become(handler(exchanges, requests - id))

    case ResponseFromExchange(id, ex, isAuthorized) =>
      log.info(s"<<< Authorization response for $id from $ex: $isAuthorized")

      requests.get(id).foreach { r =>

        val left = r.waitingForExchanges - ex

        if (isAuthorized) {
          log.info(s"!!! Authorization request $id confirmed by $ex")
          r.timeout.cancel()
          context.become(handler(exchanges, requests - id))
          r.sender ! Authorized(id, ex)
        } else if (left.isEmpty) {
          log.info(s"!!! Authorization request $id rejected by all known parties")
          r.timeout.cancel()
          context.become(handler(exchanges, requests - id))
          r.sender ! Rejected(id)
        } else {
          val updatedRequest = r.copy(waitingForExchanges = left)
          context.become(handler(exchanges, requests.updated(id, updatedRequest)))
        }
      }
  }
}
