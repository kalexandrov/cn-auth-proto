name := "cn-auth-proto"

lazy val commonSettings = Seq(
  organization := "com.newmotion",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.12.4",
  javaOptions ++= Seq(
    "-Xmx256m"
  ),
  fork in run := true,
  libraryDependencies := Seq(
    "com.typesafe.akka"   %% "akka-actor"     % "2.4.14",
    "com.spingo"          %% "op-rabbit-core" % "2.0.0"
  )
)

lazy val cn = (project in file("cn"))
  .settings(commonSettings ++ Seq(
    connectInput in run := true
  ))

lazy val ex1 = (project in file("ex1"))
  .settings(commonSettings)

lazy val ex2 = (project in file("ex2"))
  .settings(commonSettings)
