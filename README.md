# CN Public Authorization Prototype

This project consists of multiple apps:
- *cn* - application issuing authorization requests
- *ex1* - example exchange 1 (code EX1)
- *ex2* - example exchange 2 (code EX2)

There are two main ideas implemented:

### heartbeats

Exchanges inform CN they're alive by publishing messages (consisting of their name) to fanout exchange.
CN keeps internal list of alive Exchanges and publishes this list internally.

### authorization requests

Whenever some token has to be authorized AutorizationActor sends the request to direct exchange using one of routing keys type:
- _all_ when it's unknown which Exchange handles requests for this particular token
- _ex*code*_ when we know the Exchange we have to ask (not completely implemented in this proto except for routing)

In both cases we know a set of Exchanges we have to wait responses from: either all of alive Exchanges or a specific one.

Every Exchange responds to each request with either authorized or rejected message.

Whenever something goes wrong (like suddenly failing Exchange) timeout should handle this by rejecting the request.

## Running

Make sure you have RMQ broker running locally (listening on 5672) and that it has `cn-auth` virtual host.

From within SBT:
```bash
ex1/bgRun # to run Exchange 1 in background mode

ex2/bgRun # to run Exchange 2 in background mode

cn/run # to run the in interactive mode
```

To send authorization request just type any string and press ENTER.

Requests starting with specific Exchange code will get authorized by that Exchange.

Sending empty string will result in CN application shutdown.

