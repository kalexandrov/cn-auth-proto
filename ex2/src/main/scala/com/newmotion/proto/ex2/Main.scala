package com.newmotion.proto.ex2

import akka.actor._
import com.rabbitmq.client.Address
import com.spingo.op_rabbit.Directives._
import com.spingo.op_rabbit._
import org.slf4j.LoggerFactory

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.duration._

object Main extends App {
  val log = LoggerFactory.getLogger(getClass)

  val appName = "EX2"

  log.info(s"Starting $appName")

  implicit val actorSystem = ActorSystem(appName)

  val connectionParams = ConnectionParams(
    hosts = new Address("localhost", 5672) :: Nil,
    connectionTimeout = 30000,
    username = "guest",
    password = "guest",
    virtualHost = "cn-auth",
    ssl = false
  )
  val rabbitControl = actorSystem.actorOf(
    Props(new RabbitControl(connectionParams)),
    "rabbit-control"
  )

  val heartbeat = Publisher.exchange(Exchange.fanout("heartbeat"))
  actorSystem.scheduler.schedule(0.seconds, 5.seconds, rabbitControl, Message(appName, heartbeat))

  implicit val recoveryStrategy = RecoveryStrategy.none

  val authResponse = Publisher.exchange(Exchange.fanout("auth-response"))
  Subscription.run(rabbitControl) {
    channel(qos=3) {
      consume(Binding.direct(queue(s"auth-request-${appName.toLowerCase}"), Exchange.direct("auth-request"), List(appName, "all"))) {
        body(as[String]) { id =>
          if (id.startsWith(appName)) {
            rabbitControl ! Message(s"$id,$appName,yes", authResponse)
          } else {
            rabbitControl ! Message(s"$id,$appName,no", authResponse)
          }
          ack
        }
      }
    }
  }

  Await.ready(actorSystem.whenTerminated, Duration.Inf)

  actorSystem.terminate()
}
